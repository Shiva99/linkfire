# Introduction

This bitbucket repository practical is central to CI/CD, Infrastructure as Code & Cloud-based Infrastructure.

## Work and flow

Take simple html with javascript app that runs on nginx web server. Application calculates factorial of a positive intiger.
Made bitbucket pipeline for automation of CI/CD. It builds docker image from the Dockerfile.  

Gets docker login credentials based on AWS access and scecret keys defined as repository variables.
Pushes image to AWS ECR.

Any code push to the main branch will deploy the application AWS ecr.
## IaC and EKS
Included Infrastructure as code with Terraform code for basic simple network on AWS with 1 vpc, 4 subnets, 1 internet gateway, 1 NAT gateway, aws routes and associations. 
There is also eks cluster code that could create eks cluster. 
Whenk k8s manifests can be deployed using kubectl. A separte namespace is created. A load balancer service is created. With the endpoint of LB at port 80. Service can be reached. Port is open from security group. A deployment with two pods with container that runs docker image that is push to ECR repository by above CI/CD pipeline.
k8s cluster is not created as it involves cost.


